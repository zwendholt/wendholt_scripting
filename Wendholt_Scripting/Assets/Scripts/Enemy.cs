﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static int enemyCount = 0;
    private int health = 130;
    private bool alive;
    // Start is called before the first frame update
    void Start()
    {
        enemyCount += 1;
        alive = true;
        print("There are currently: " + enemyCount + " enemies.");
    }

    void OnEnable()
    {
        Player.attackEvent += hit;
    }

    void OnDisable() 
    {
        Player.attackEvent -= hit;
    }

    void hit(int damage) 
    {
        health -=damage;
        if (health <= 0 && alive == true)
        {
            print("I am dead");
            alive = false;
        }
    }

}
