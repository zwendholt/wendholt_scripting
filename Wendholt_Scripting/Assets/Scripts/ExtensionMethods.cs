﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static List<Weapon> EmptyHands(this List<Weapon> hand)
    {
        // empty the list for the player
        return new List<Weapon>();
    }
}
