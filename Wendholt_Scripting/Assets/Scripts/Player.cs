﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    
    public Sword sword;
    private List<Weapon> hand = new List<Weapon>();
    private Dictionary<string, int> stats = new Dictionary<string, int>();
    
    // this delegate will either be lighting or water
    delegate int MagicDelegate();
    MagicDelegate magicDelegate;

    // the event to send to the enemy for hit
    public delegate void ClickAction(int damage);
    public static event ClickAction attackEvent;

    // text objects
    public Text strengthText;
    public Text healthText;
    public Text armourText;
    public Text levelText;
    public Text handText;

    private int attackTotal;

    //player stats
    private int playerHealth = 100;
    [Range(0,100)]
    public int playerStrength = 10;
    private int playerArmour = 5;
    private int experience;
    private int Level
    {
        get
        {
            return experience/ 10;
        }
        set
        {
            experience = value* 10;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        // testing the inventory
        hand.Add(sword);
        experience = 20;
        // testing the dictionary
        stats.Add("strength", playerStrength);
        stats.Add("health", playerHealth);
        stats.Add("armour", playerArmour);
        stats.Add("level", Level); 
        magicDelegate = lighting;       
    }

    // Update is called once per frame
    void Update()
    {
        stats["health"] = playerHealth;
        stats["level"] = Level;
        updateText();

        if(Input.GetKeyDown("space"))
        {
            if(hand.Count == 0)
            {
                attackEvent(attack());
            }
            else if(hand.Count == 1)
            {
                attackEvent(attack(hand[0]));
            }
            else if(hand.Count == 2)
            {
               attackEvent(attack(hand[0], hand[1]));
            }
        }

        //changing the attack type to magic.
        if(Input.GetKeyDown(KeyCode.L))
        {
            print("equiped lighting.");
            magicDelegate = lighting;
        }

        //changing the attack type to magic.
        if(Input.GetKeyDown(KeyCode.W))
        {
            print("equiped water.");
            magicDelegate = water;
        }

        // adding another weapon to the other hand
        if(Input.GetKeyDown(KeyCode.A) && hand.Count < 2)
        {
            hand.Add(sword);
        }
        
        //returns the sword in the left hand.
        if(Input.GetKeyDown(KeyCode.B) && hand.Count >0)
        {
            print(hand[0].getName());
        }

        // cause the magic attack for the enemies
        if(Input.GetKeyDown(KeyCode.M))
        {
            attackEvent(magicDelegate());
        }

        //empty the players hands
        if(Input.GetKeyDown(KeyCode.E))
        {
            print("your hands are now empty");
            hand = hand.EmptyHands();
            //hand = new List<Weapon>();
        }

    }

    void updateText()
    {
        strengthText.text = "Strength: " + stats["strength"];
        healthText.text = "Health: " + stats["health"];
        armourText.text = "Armour: " + stats["armour"];
        levelText.text = "Level: " + stats["level"];
        if(hand.Count != 0)
        {
            handText.text = hand.Count == 1 ? "Hand 1: " + hand[0].getType() : "Hand 1: " + hand[0].getType() + " Hand 2: " + hand[1].getType();
        }
        else
        {
            handText.text = "hand: ";
        }
    }

    // physical attack with one sword equipped.
    private int attack(Weapon weaponOne)
    {
        attackTotal = stats["strength"] + hand[0].getDamage();
        print("one " + hand[0].getType() + " in left hand");
        print("attack total = " + attackTotal);
        return attackTotal;
    }

    // physical attack with two swords equipped.
    private int attack(Weapon weaponOne, Weapon weaponTwo)
    {
        attackTotal = stats["strength"] + hand[0].getDamage() + hand[1].getDamage();
        print(hand[0].getType() + " in left and " + hand[1].getType() + " in right");
        print("attack total = " + attackTotal);
        return attackTotal;
    }

    // attacking with bare fists.
    private int attack()
    {
        attackTotal = stats["strength"];
        print("only using hands");
        print("attack total = "+ attackTotal);
        return attackTotal;
    }

    // magic attacks
    private int lighting()
    {
        print("used lightning.");
        print("caused 40 damage.");
        return 40;
    }

    private int water()
    {
        print("player used splash.");
        print("caused 2 damage.");
        return 2;
    }

}
