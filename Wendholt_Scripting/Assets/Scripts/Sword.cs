﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Weapon
{
    // Start is called before the first frame update
    void Start()
    {
      damage = 10;  
      type = "sword";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override string getName()
    {
      return "this is one sweet sword. ";
    }
}
