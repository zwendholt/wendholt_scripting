﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    protected int damage;
    protected string type;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int getDamage()
    {
        return damage;
    }

    public string getType()
    {
        return type;
    }
    
    public virtual string getName()
    {
        return "this is just a generic weapon. ";
    }
}
